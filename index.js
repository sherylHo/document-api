import express from "express";
import bodyParser from "body-parser";
import cors from "cors";

import documentsRoutes from "./routes/documents.js";
import userRoutes from "./routes/user.js";

const app = express();
const PORT = 5000;

app.use(bodyParser.json());
app.use(cors());

app.use("/documents", documentsRoutes);
app.use("/", userRoutes);

app.get("/", (req, res) => res.send("hello from homepage"));

app.listen(PORT, () =>
  console.log(`Server running on port: http://localhost:${PORT}`)
);
