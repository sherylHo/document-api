import { v4 as uuidv4 } from "uuid";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";

let users1 = [
  {
    username: "ad",
    password: "$2b$10$Y6.BDSZ615HXDjfH0kp15eaXVe//N2Oc3s8Z66S8eW1yHZfZnBJlS", // 1
    id: "50c45a97-8cce-4fc9-b946-d4276812b418",
  },
  {
    username: "ad1",
    password: "$2b$10$uFgdkv6fHrEkPYptgPwQOOCxNbUCK3pq6xXXgY4b6pdjZ.JHZaBOu", //123
    id: "9ce55e54-2760-4194-8722-a59c2b28c394",
  },
];

const JWT_SECRET = "ksdjfjknfksnfkjdf#@@*&/";

export const registerUser = async (req, res) => {
  const { username, password } = req.body;
  const newPassword = await bcrypt.hash(password, 10);

  const user = users1.find((item) => item.username === username);

  if (user) {
    return res.json({ status: "error", error: "Username or password exist" });
  } else {
    users1.push({
      username,
      password: newPassword,
      id: uuidv4(),
    });
    return res.json({ status: "ok" });
  }
};

export const login = async (req, res) => {
  const { username, password } = req.body;

  const user = users1.find((item) => item.username === username);

  if (!user) {
    return res.json({ status: "error", error: "Invalid username/password" });
  }
  if (await bcrypt.compare(password, user.password)) {
    const token = jwt.sign(
      { id: user.id, username: user.username },
      JWT_SECRET
    );

    return res.json({ status: "ok", user: { token, username: user.username } });
  }
  return res.json({ status: "error", error: "Invalid username/password" });
};
