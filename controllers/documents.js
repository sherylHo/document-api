import { v4 as uuidv4 } from "uuid";

let documents = [
  {
    Name: "John 123",
    Email: "john@gmail.com",
    Phone_Number: "0123456789",
    Address: "Test",
    KTP_Number: 1,
    NPWP_Number: 111,
    Passport_Number: 1,
    id: "11b8787e-adc0-41e3-be5e-3381968ea07d",
  },
  {
    Name: "John",
    Email: "john@gmail.com",
    Phone_Number: "0123456789",
    Address: "Test",
    KTP_Number: 1,
    NPWP_Number: 111,
    Passport_Number: 1,
    id: "de4f6713-9fa5-4c9c-b49f-71572cab8b65",
  },
  {
    Name: "Jane",
    Email: "jane@gmail.com",
    Phone_Number: "0928765432",
    Address: "Test",
    KTP_Number: 12,
    NPWP_Number: 111,
    Passport_Number: 2,
    id: "b9bc8711-4a2b-465d-a198-f65cd7d95617",
  },
  {
    Name: "Jane 2",
    Email: "jane2@gmail.com",
    Phone_Number: "0928765432",
    Address: "Test",
    KTP_Number: 123,
    NPWP_Number: 111,
    Passport_Number: 23,
    id: "b50e577f-b981-4372-8133-b7eb4e2fe1fa",
  },
  {
    Name: "Mary",
    Email: "mary@gmail.com",
    Phone_Number: "0928765432",
    Address: "Test",
    KTP_Number: 3,
    NPWP_Number: 111,
    Passport_Number: 3,
    id: "cd473832-a479-4d07-b1ab-6fe643581170",
  },
  {
    Name: "Mary 2",
    Email: "mary@gmail.com",
    Phone_Number: "0928765432",
    Address: "Test",
    KTP_Number: 4,
    NPWP_Number: 111,
    Passport_Number: 34,
    id: "e528435d-2609-4757-9914-cbd041fe6041",
  },
  {
    Name: "Peter",
    Email: "peter@gmail.com",
    Phone_Number: "0928765432",
    Address: "Test",
    KTP_Number: 5,
    NPWP_Number: 111,
    Passport_Number: 5,
    id: "2860ae1f-6deb-486b-abaf-9305741b6d61",
  },
  {
    Name: "Peter 2",
    Email: "peter2@gmail.com",
    Phone_Number: "0928765432",
    Address: "Test",
    KTP_Number: 6,
    NPWP_Number: 111,
    Passport_Number: 6,
    id: "8f1e26d2-37c4-4155-b7e0-422bf686f3ee",
  },
  {
    Name: "Peter 3",
    Email: "peter3@gmail.com",
    Phone_Number: "0928765432",
    Address: "Test",
    KTP_Number: 7,
    NPWP_Number: 111,
    Passport_Number: 7,
    id: "45440115-7379-47fc-8c6d-148d28bebaa9",
  },
  {
    Name: "Jade",
    Email: "jade@gmail.com",
    Phone_Number: "0928765432",
    Address: "Test",
    KTP_Number: 8,
    NPWP_Number: 111,
    Passport_Number: 8,
    id: "0454b89e-52d0-4b7e-9143-0d3d02f14e61",
  },
  {
    Name: "Jade 2",
    Email: "jade@gmail.com",
    Phone_Number: "0928765432",
    Address: "Test",
    KTP_Number: 9,
    NPWP_Number: 111,
    Passport_Number: 9,
    id: "37d6a7da-0489-4224-9a45-bdcb8e0d76a6",
  },
  {
    Name: "Jade 3",
    Email: "jade3@gmail.com",
    Phone_Number: "0928765432",
    Address: "Test",
    KTP_Number: 10,
    NPWP_Number: 111,
    Passport_Number: 10,
    id: "0f0d5c29-56cd-45b8-b19c-1971251a3cc1",
  },
];

export const getDocuments = (req, res) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.send(documents);
};

export const createDocument = (req, res) => {
  const user = req.body;

  documents.push({ ...user, id: uuidv4() });

  res.send(`User with the name ${user.firstName} added to db`);
};

export const getDocumentDetail = (req, res) => {
  const { id } = req.params;
  const foundDocument = documents.find((document) => document.id === id);

  res.send(foundDocument);
};

export const deleteDocument = (req, res) => {
  const { id } = req.params;

  documents = documents.filter((document) => document.id !== id);

  res.send(`Document with the ${id} deleted from DB`);
};

export const updateDocument = (req, res) => {
  const { id } = req.params;
  const {
    Name,
    Email,
    Phone_Number,
    Address,
    KTP_Number,
    NPWP_Number,
    Passport_Number,
  } = req.body;
  console.log(req.body);
  const document = documents.find((document) => document.id === id);
  console.log("document", document);

  if (Name) document.Name = Name;
  if (Email) document.Email = Email;
  if (Phone_Number) document.Phone_Number = Phone_Number;
  if (Address) document.Address = Address;
  if (KTP_Number) document.KTP_Number = KTP_Number;
  if (NPWP_Number) document.NPWP_Number = NPWP_Number;
  if (Passport_Number) document.Passport_Number = Passport_Number;

  res.json({ status: "ok" });
};
