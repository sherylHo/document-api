import express from "express";

import {
  getDocuments,
  createDocument,
  getDocumentDetail,
  deleteDocument,
  updateDocument,
} from "../controllers/documents.js";
const router = express.Router();

router.get("/", getDocuments);
router.post("/", createDocument);
router.get("/:id", getDocumentDetail);
router.delete("/:id", deleteDocument);
router.put("/:id", updateDocument);

export default router;
